
import 'package:sailor/sailor.dart';

//  Routes.sailor.navigate(Routes.helpContactPageRoute,
//                   params: {"code": hospital?.code ?? ""});

class Routes {
  static const homePageRoute = '/home';

  static final Sailor sailor = Sailor();

  static void createRoutes() {
    sailor.addRoutes(
      [
        // SailorRoute(
        //   name: homePageRoute,
        //   builder: (_, __, ___) => DashboardPage(),
        // ),
   
        // SailorRoute(
        //   name: travelItemDetailPageRoute,
        //   builder: (_, args, params) =>
        //       DetailTravelItem(id: params.param<int>('id')),
        //   params: [SailorParam<int>(name: 'id', defaultValue: null)],
        // ),
        // SailorRoute(
        //   name: hospitalPackage,
        //   builder: (_, __, params) =>
        //       HospitalPackagePage(idService: params.param<String>('id')),
        //   params: [SailorParam<String>(name: 'id', defaultValue: null)],
        //   routeGuards: [
        //     SailorRouteGuard.simple((_, __, params) async {
        //       return params.param<String>('id') != null;
        //     }),
        //   ],
        // ),
        
        // SailorRoute(
        //   name: detailPackagePageRoute,
        //   builder: (_, args, params) =>
        //       DetailPackage(id: params.param<String>("id")),
        //   params: [SailorParam<String>(name: 'id', defaultValue: null)],
        // ),
        // // builder: (_, args, ___) => WidgetTest()),
        // SailorRoute(
        //     name: registerPageRoute, builder: (_, args, ___) => RegisterPage()),
        // SailorRoute(
        //     name: passwordConfirmPage,
        //     builder: (_, args, ___) => PasswordPage()),
        // SailorRoute(
        //   name: webViewRoute,
        //   builder: (_, args, ___) => WebViewWidget(args: args),
        // ),
        // SailorRoute(
        //   name: overViewJapanRoute,
        //   builder: (_, __, ___) => OverViewJapan(),
        // ),
        // SailorRoute(
        //   name: relationVNJP,
        //   builder: (_, __, ___) => Relation(),
        // ),
        // SailorRoute(
        //   name: detailContent,
        //   builder: (_, args, ___) => DetailContent(args as DetailContentArgs),
        // ),
        // SailorRoute(
        //   name: medicalJapan,
        //   builder: (_, __, ___) => MedicalJapan(),
        // ),
        // SailorRoute(
        //   name: embassyDashBoardRoute,
        //   builder: (_, __, ___) => DashboardEmbassy(),
        // ),
        // SailorRoute(
        //   name: embassyOverViewRoute,
        //   builder: (_, args, params) => OverViewEmbassy(
        //     from: params.param<String>('from'),
        //   ),
        //   params: [SailorParam<String>(name: 'from', defaultValue: null)],
        // ),
        // SailorRoute(
        //   name: stepProcedureDetailRouter,
        //   builder: (_, __, params) => StepProcedureDetailPage(
        //     step: params.param<int>("step"),
        //     content: params.param<String>("content"),
        //     title: params.param<String>("title"),
        //   ),
        //   params: [
        //     SailorParam<int>(name: 'step', defaultValue: null),
        //     SailorParam<String>(name: 'content', defaultValue: ""),
        //     SailorParam<String>(name: 'title', defaultValue: ""),
        //   ],
        // ),
        
      ],
    );
  }
}
