class Constants {
  static const String IMG_NOT_FOUND = "assets/images/notfound.png";

  static const String PROTOCOL = "https://";
  static const String DOMAIN = "kcb-dev.tetvietaic.com";
  static const String SERVER = "${PROTOCOL}kcb-dev.tetvietaic.com";
  static const String HEALTH_CARE_PACKAGES = "/api/packageServices";
  static const String HEALTH_CARE_PACKAGES_HOSPITAL =
      "/api/packageServices/{idPackage}";
  static const String LIST_HOSPITAL = "/api/hospitals";
  static const String DETAIL_HOSPITAL = "/api/hospitals/{id}";
  static const String SPECIALTIES_HOSPITAL = "/api/specialties";
  static const String INTERNATIONAL_HOSPITAL =
      "/api/international-relationships";
  static const String DETAIL_PACKAGE = "/api/health_care_packages/{id}";
  static const String URL_GET_NEWS_JAPAN = "/api/news";
  static const String PACKAGE_HOSPITAL =
      "api/health_care_packages/get-by-hospital/{id}";
  static const String PATH_GET_NEWS_JAPAN = "/api/news";
  static const String PATH_GET_COOPERATION = "/api/cooperation";
  static const String PATH_GET_EMBASSY_JAPAN = "/api/embassy";
  static const String PATH_INTRODUCTION_JAPAN = "/api/introduction";
  static const String TRAVEL_TOURS = "/api/travelTours/";
  static const String TRAVEL_TOUR_DETAIL = "/api/travelTours/{id}";

  static const String REQUEST_SUPPORT = "/api/requestSupports";
  static const String PATH_OVERVIEW_JAPAN = "/api/overview";
  static const String PATH_COOPERATE_PROJECT = "/api/cooperation/projects";
  static const String PATH_COOPERATE_ORGANIZATION =
      "/api/cooperation/cooperative-organizations";
  static const String PATH_WEATHER = "/api/weathers";

  static const String EMBASSY = "/api/embassy";
  static const String SPECIALIST_DOCTOR = "/api/specialist-doctors";

  static const String PATH_HISTORY_RELATION = "/api/cooperation/history";
  static const String PATH_NEW_RELATION = "/api/news";
  static const String JAPAN_MEDICAL = "/api/medicals";

  static const String PATH_CURRENCY_RATE = "/api/rates";
  static const String PATH_CITY_WITH_COUNTRY = "/api/cities";
  static const String PATH_TYPE_HOSPITAL = "/api/hospital-types";

  static String urlConnect(String pathUrl) {
    return "$PROTOCOL$DOMAIN$pathUrl";
  }

  static String getUrlWithParam(String pathUrl, Map<String, String> param) {
    String url;
    param.forEach((key, value) => {url = pathUrl.replaceAll("{$key}", value)});
    return url;
  }
}
