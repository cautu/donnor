import 'package:intl/intl.dart';
import 'package:timeago/timeago.dart' as timeago;

class Utils {
  static String formatTimeAgo(String timeZone) {
    final dateParse = DateTime.parse(timeZone);
    timeago.setLocaleMessages('vi', CustomVi());
    return timeago.format(dateParse, locale: 'vi');
  }

  static String formatDate(String timeZone, {String format = "dd-MM-yyyy"}) {
    if (timeZone == null) return "Chưa xác định";
    final dateParse = DateTime.parse(timeZone);
    DateFormat formatter = DateFormat(format);
    return formatter.format(dateParse);
  }

  static String convertMounth(int mounth) {
    if (mounth == 1) return "T.Giêng";
    if (mounth == 12) return "T.Chạp";
    return "Tháng $mounth";
  }

  static String getFormatDate() {
    final now = DateTime.now();
    final dateFormat = DateFormat("dd/MM/yyyy").format(now);
    return dateFormat;
  }

  static String getDayOfWeek() {
    final now = DateTime.now();
    final dayOfWeek = DateFormat('EEEE').format(now);
    return convertDayOfWeekToVi(dayOfWeek);
  }

  static String formatToCurrentcy(int moneyEmount) {
    final formatCurrency = new NumberFormat(",### đ", "vi");
    return moneyEmount != null ? formatCurrency.format(moneyEmount) : "";
  }

  static String convertDayOfWeekToVi(String day) {
    switch (day) {
      case "Monday":
        return "Thứ 2";
      case "Tuesday":
        return "Thứ 3";
      case "Wednesday":
        return "Thứ 4";
      case "Thursday":
        return "Thứ 5";
      case "Friday":
        return "Thứ 6";
      case "Saturday":
        return "Thứ 7";
      default:
        return "Chủ nhật";
    }
  }

  static String validateEmail(String value) {
    Pattern pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regex = new RegExp(pattern);
    if (!regex.hasMatch(value))
      return 'Email không đúng định dạng';
    else
      return null;
  }

  static String phoneNumberValidator(String value) {
    Pattern pattern = r'^(?:[+0]9)?[0-9]{10}$';
    RegExp regex = new RegExp(pattern);
    if (!regex.hasMatch(value))
      return 'Số điện thoại không đúng định dạng';
    else
      return null;
  }
}

class CustomVi extends timeago.ViMessages {
  @override
  String prefixAgo() => '';
  @override
  String prefixFromNow() => '';
  @override
  String suffixAgo() => 'trước';
  @override
  String suffixFromNow() => 'nữa';
  @override
  String lessThanOneMinute(int seconds) => 'một thoáng';
  @override
  String aboutAMinute(int minutes) => 'khoảng một phút';
  @override
  String minutes(int minutes) => '$minutes phút';
  @override
  String aboutAnHour(int minutes) => 'khoảng 1 tiếng';
  @override
  String hours(int hours) => '$hours tiếng';
  @override
  String aDay(int hours) => 'một ngày';
  @override
  String days(int days) => '$days ngày';
  @override
  String aboutAMonth(int days) => 'khoảng 1 tháng';
  @override
  String months(int months) => '$months tháng';
  @override
  String aboutAYear(int year) => 'khoảng 1 năm';
  @override
  String years(int years) => '$years năm';
  @override
  String wordSeparator() => ' ';
}

class CustomViShort extends timeago.ViShortMessages {
  @override
  String prefixAgo() => '';
  @override
  String prefixFromNow() => '';
  @override
  String suffixAgo() => '';
  @override
  String suffixFromNow() => '';
  @override
  String lessThanOneMinute(int seconds) => 'bây giờ';
  @override
  String aboutAMinute(int minutes) => '1 ph';
  @override
  String minutes(int minutes) => '$minutes ph';
  @override
  String aboutAnHour(int minutes) => '~1 h';
  @override
  String hours(int hours) => '$hours h';
  @override
  String aDay(int hours) => '~1 ngày';
  @override
  String days(int days) => '$days ngày';
  @override
  String aboutAMonth(int days) => '~1 tháng';
  @override
  String months(int months) => '$months tháng';
  @override
  String aboutAYear(int year) => '~1 năm';
  @override
  String years(int years) => '$years năm';
  @override
  String wordSeparator() => ' ';
}
