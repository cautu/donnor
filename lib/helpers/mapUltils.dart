import 'package:url_launcher/url_launcher.dart';
import 'dart:io';

class MapUtils {
  MapUtils._();

  static Future<void> openMap(double latitude, double longtitude) async {
    String urlMap = '';

    if (Platform.isIOS) {
      urlMap = 'https://maps.apple.com/?q=$latitude,$longtitude';
    } else {
      urlMap =
          'https://www.google.com/maps/search/?api=1&query=$latitude,$longtitude';
    }
    if (await canLaunch(urlMap)) {
      await launch(urlMap);
    } else {
      throw 'Could n ot open the map.';
    }
  }
}
