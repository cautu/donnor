import 'package:url_launcher/url_launcher.dart';

class CallsAndMessagesService {
  static void call(String number) {
    final numberExact = number.trim().replaceAll(" ", "");
    launch("tel:$numberExact");
  }

  static void sendSms(String number) {
    final numberExact = number.trim().replaceAll(" ", "");
    launch("sms:$numberExact");
  }

  static void sendEmail(String email) {
    final emailExact = email.trim().replaceAll(" ", "");
    launch("mailto:$emailExact");
  }

  static void launchURL(String url) => launch(url);
}
