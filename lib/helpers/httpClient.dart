import 'dart:convert';

import 'package:http/http.dart' as http;

class HttpClient {
  static Map<String, String> defaultHeader = {
    'Content-type': 'application/json',
    'Accept': 'application/json',
    'Authorization': '',
  };

  static Map<String, String> getDefaultHeader() {
    final String _token =
        'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1ZTYwNGVkNmY1NDVlOTYyYzBkMGZmMmEiLCJwaG9uZU51bWJlciI6Iis4NDk2MTEwNjcyNSIsIm5hbWUiOiJUdSBBbiBTaW5oIiwicm9sZXMiOlsiYWRtaW4iXSwiaWF0IjoxNTgzMzcyMzc4LCJleHAiOjE1ODU5NjQzNzh9.OKIt1kXzbcDZpbFwS_vTPj95TWTPTSBz4NK_RZOCjIg';
    defaultHeader['Authorization'] = 'Bearer $_token';
    return defaultHeader;
  }

  static Future<dynamic> post(String url,
      {Map<String, String> headers, body, Encoding encoding}) async {
    return performRequest(
      (Map<String, String> refreshedHeaders) {
        return http.post(
          url,
          headers: refreshedHeaders,
          body: json.encode(body),
          encoding: encoding,
        );
      },
      headers: headers,
    );
  }

  static Future<dynamic> get(dynamic url, {Map<String, String> headers}) async {
    return performRequest(
      (Map<String, String> refreshedHeaders) {
        return http.get(
          url,
          headers: refreshedHeaders,
        );
      },
      headers: headers,
    );
  }

  static Future<http.Response> delete(String url,
      {Map<String, String> headers}) async {
    return performRequest(
      (Map<String, String> refreshedHeaders) {
        return http.delete(
          url,
          headers: refreshedHeaders,
        );
      },
      headers: headers,
    );
  }

  static Future<http.Response> put(String url,
      {Map<String, String> headers, body, Encoding encoding}) async {
    return performRequest(
      (Map<String, String> refreshedHeaders) {
        return http.put(
          url,
          headers: refreshedHeaders,
          body: json.encode(body),
          encoding: encoding,
        );
      },
      headers: headers,
    );
  }

  static Future<http.Response> head(String url,
      {Map<String, String> headers, body, Encoding encoding}) async {
    return performRequest(
      (Map<String, String> refreshedHeaders) {
        return http.head(
          url,
          headers: refreshedHeaders,
        );
      },
      headers: headers,
    );
  }

  static Future<http.Response> patch(String url,
      {Map<String, String> headers, body, Encoding encoding}) async {
    return performRequest(
      (Map<String, String> refreshedHeaders) {
        return http.patch(
          url,
          headers: refreshedHeaders,
          body: json.encode(body),
          encoding: encoding,
        );
      },
      headers: headers,
    );
  }

  static Future<dynamic> performRequest(
      Future<http.Response> Function(Map<String, String>) request,
      {Map<String, String> headers}) async {
    final http.Response response = await request(headers ?? getDefaultHeader());
    if (response.statusCode == 200) {
      final jsonDecode = json.decode(utf8.decode(response.bodyBytes));
      if (jsonDecode['success'] != null && jsonDecode['success']) {
        return jsonDecode;
      }
      return response;
    }
    return response;
  }
}
