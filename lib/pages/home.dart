import 'package:flutter/material.dart';
import './test_auto_suggest/auto_suggest.dart';
import './searchable_dropdown/searchable_dropdown.dart';
import "./carsousel_slider/carsousel_slider.dart";
import "./carsousel_slider/simple_slide_widget.dart";
import "./multi_select/multi_select.dart";
import 'package:flutter_rounded_date_picker/rounded_picker.dart';
import 'package:flutter/cupertino.dart';

class Home extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("home"),
        ),
        body: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              AutoCompleteDemo(),
              Text('fdfdf'),
              Test(),
              CarsouselSliderWidget(),
              SimpleSLideWidget(),
              Text('fdfdf'),
              Text('fdfdf'),
              Text('fdfdf'),
              MultiSelect(),
              Text('5454'),
              Text('433'),
              FlatButton(
                color: Colors.red,
                child: Text('choose date'),
                onPressed: () async {
                  // DateTime newDateTime = await showRoundedDatePicker(
                  //   initialDate: DateTime.parse("2020-03-26 00:07:00.000"),
                  //   // locale: Locale('vi', "VN"),
                  //   context: context,
                  //   // initialDate: DateTime.now(),
                  //   firstDate: DateTime(DateTime.now().year - 1),
                  //   lastDate: DateTime(DateTime.now().year + 1),
                  //   // theme: ThemeData(primarySwatch: Colors.red),
                  //   theme: ThemeData.dark(),
                  //   borderRadius: 4,
                  // );
                  // print(newDateTime.toString());
                  CupertinoRoundedDatePicker.show(
                    context,
                    fontFamily: "Mali",
                    textColor: Colors.white,
                    background: Colors.red[300],
                    borderRadius: 16,
                    initialDatePickerMode: CupertinoDatePickerMode.date,
                    onDateTimeChanged: (newDateTime) {
                      //
                      print(newDateTime);
                    },
                  );
                },
              )
            ],
          ),
        ));
  }
}
