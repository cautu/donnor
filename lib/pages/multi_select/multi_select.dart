import 'package:flutter/material.dart';
import 'package:multiselect_formfield/multiselect_formfield.dart';

class MultiSelect extends StatefulWidget {
  @override
  _MultiSelectState createState() => _MultiSelectState();
}

class _MultiSelectState extends State<MultiSelect> {
  List _myActivities = [];
  String _myActivitiesResult = "";
  final formKey = new GlobalKey<FormState>();
  _saveForm() {
    var form = formKey.currentState;

    if (form.validate()) {
      form.save();
      setState(() {
        _myActivitiesResult = _myActivities.toString();
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Center(
      child: Form(
        key: formKey,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Container(
              padding: EdgeInsets.all(16),
              child: MultiSelectFormField(
                autovalidate: true,
                titleText: '',
                validator: (value) {
                  if (value == null || value.length == 0) {
                    return 'Please select one or more options';
                  }
                  return null;
                },
                dataSource: [
                  {
                    "display": "Running1",
                    "value": "Running",
                  },
                  {
                    "display": "Climbing1",
                    "value": "Climbing",
                  },
                  {
                    "display": "Walking1",
                    "value": "Walking",
                  },
                  {
                    "display": "Swimming1",
                    "value": "Swimming1",
                  },
                  {
                    "display": "Soccer Practice1",
                    "value": "Soccer Practice",
                  },
                  {
                    "display": "Baseball Practice1",
                    "value": "Baseball Practice",
                  },
                  {
                    "display": "Football Practice1",
                    "value": "Football Practice1",
                  },
                ],
                textField: 'display',
                valueField: 'value',
                okButtonLabel: 'OK',
                cancelButtonLabel: 'CANCEL',
                // required: true,
                hintText: 'Please choose one or more1',
                value: _myActivities,
                trailing: Text('ffd'),
                leading: Text('g'),
                onSaved: (value) {
                  if (value == null) return;
                  setState(() {
                    _myActivities = value;
                  });
                },
              ),
            ),
            Container(
              padding: EdgeInsets.all(8),
              child: RaisedButton(
                child: Text('Save'),
                onPressed: _saveForm,
              ),
            ),
            Container(
              padding: EdgeInsets.all(16),
              child: Text(_myActivitiesResult),
            )
          ],
        ),
      ),
    );
  }
}
