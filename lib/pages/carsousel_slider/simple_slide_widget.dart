import 'package:flutter/material.dart';
import 'package:bright_sparqe/pages/simple_slider/simple_slider.dart';

class SimpleSLideWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return ImageSliderWidget(
      imageUrls: [
        "https://vevietnamairline.com/Img.ashx?636547984689865774.jpg",
        "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQgy3JuuFXGGgach-tIuc3rQLIzq2F2JcU4VuD9L948ds16CWTk",
        "https://mytourcdn.com/upload_images/Image/Articles%20Location/B%E1%BA%A3n%20P%C3%A1c%20Ng%C3%B2i/1.jpg"
      ],
      imageBorderRadius: BorderRadius.circular(0.0),
    );
  }
}
