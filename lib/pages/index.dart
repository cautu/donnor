import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class DashboardPage extends StatefulWidget {
  DashboardPage({Key key}) : super(key: key);

  @override
  _DashboardPageState createState() => _DashboardPageState();
}

class _DashboardPageState extends State<DashboardPage> {
  int _selectedIndex = 0;

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  final List<Widget> _children = [
    // HomePage(),
    // NotifyPage(),
    // DashboardUserPage(),
    // // Text('y kien'),
    // ScheduleManagerPage(),
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: AnimatedSwitcher(
        duration: const Duration(milliseconds: 1000),
        child: _children[_selectedIndex],
        switchInCurve: Curves.fastLinearToSlowEaseIn,
        switchOutCurve: Curves.linear,
      ),
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        items: [
          BottomNavigationBarItem(
            icon: SvgPicture.asset("assets/svg/trang_chu_off.svg"),
            activeIcon: SvgPicture.asset("assets/svg/trang_chu_on.svg"),
            title: Text('Trang chủ'),
          ),

          BottomNavigationBarItem(
            icon: SvgPicture.asset("assets/svg/thong_bao_off.svg"),
            activeIcon: SvgPicture.asset("assets/svg/thong_bao_on.svg"),
            title: Text('Thông báo'),
          ),
          BottomNavigationBarItem(
            icon: SvgPicture.asset("assets/svg/ca_nhan_off.svg"),
            activeIcon: SvgPicture.asset("assets/svg/ca_nhan_on.svg"),
            title: Text('Cá nhân'),
            // backgroundColor: Colors.red,
          ),
          // BottomNavigationBarItem(
          //   icon: Image.asset("assets/images/icon_re.png"),
          //   activeIcon: Image.asset("assets/images/y_kien.png"),
          //   title: Text('Ý kiến'),
          // ),
          BottomNavigationBarItem(
            icon: SvgPicture.asset("assets/svg/quan_ly_lich_off.svg"),
            activeIcon: SvgPicture.asset("assets/svg/quan_ly_lich_on.svg"),
            title: Text('Quản lý lịch'),
          ),
        ],
        currentIndex: _selectedIndex,
        selectedItemColor: Colors.black,
        selectedLabelStyle: TextStyle(
            fontSize: 12,
            color: Color(0xFF3A454D),
            fontWeight: FontWeight.w800),
        onTap: _onItemTapped,
      ),
    );
  }
}
