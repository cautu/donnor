import 'package:flutter/material.dart';
import './new_page.dart';
import './list_view_item.dart';

class MainNavigationWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GridView.count(
      primary: false,
      padding: const EdgeInsets.symmetric(horizontal: 25, vertical: 40),
      crossAxisSpacing: 10,
      mainAxisSpacing: 10,
      crossAxisCount: 3,
      shrinkWrap: true,
      children: <Widget>[
        OpenContainerWrapper(
          // newPage: IntroduceJapan(),
          closedBuilder: (_, function) {
            return HomeListViewItem(
              imageUrl: "assets/svg/gthieu_nhat_ban.svg",
              description: "Giới thiệu về Nhật Bản",
              width: 56,
              imageSize: 56,
              onTab: function,
            );
          },
        ),
        // HomeListViewItem(
        //   imageUrl: "assets/svg/gthieu_nhat_ban.svg",
        //   description: "Giới thiệu về Nhật Bản",
        //   width: 56,
        //   imageSize: 56,
        //   onTab: () {
        //     Routes.sailor.navigate(Routes.introduteVietNamPageRoute);
        //   },
        // ),
        OpenContainerWrapper(
          // newPage: HospitalListPage(code: "jp"),
          closedBuilder: (_, function) {
            return HomeListViewItem(
              imageUrl: "assets/svg/bv_nhat_ban.svg",
              description: "Bệnh viện Nhật Bản",
              width: 56,
              imageSize: 56,
              onTab: function,
            );
          },
        ),
        // HomeListViewItem(
        //   imageUrl: "assets/svg/bv_nhat_ban.svg",
        //   description: "Bệnh viện Nhật Bản",
        //   width: 56,
        //   imageSize: 56,
        //   onTab: () {
        //     Routes.sailor
        //         .navigate(Routes.hospitalListPageRoute, params: {"code": "jp"});
        //   },
        // ),
        OpenContainerWrapper(
          // newPage: HospitalListPage(code: "vn"),
          closedBuilder: (_, function) {
            return HomeListViewItem(
              imageUrl: "assets/svg/bv_viet_nam.svg",
              description: "Bệnh viện Việt Nam",
              width: 56,
              imageSize: 56,
              onTab: function,
            );
          },
        ),
        // HomeListViewItem(
        //   imageUrl: "assets/svg/bv_viet_nam.svg",
        //   description: "Bệnh viện Việt Nam",
        //   width: 56,
        //   imageSize: 56,
        //   onTab: () {
        //     Routes.sailor
        //         .navigate(Routes.hospitalListPageRoute, params: {"code": "vn"});
        //   },
        // ),
        OpenContainerWrapper(
          // newPage: ExtensionPage(),
          closedBuilder: (_, function) {
            return HomeListViewItem(
              imageUrl: "assets/svg/npo.svg",
              description: "Hiệp hội NPO",
              width: 56,
              imageSize: 56,
              onTab: function,
            );
          },
        ),
        // HomeListViewItem(
        //   imageUrl: "assets/svg/npo.svg",
        //   description: "Hiệp hội NPO",
        //   width: 56,
        //   imageSize: 56,
        //   onTab: () {
        //     Routes.sailor.navigate(Routes.npoOrganization);
        //   },
        // ),
        OpenContainerWrapper(
          // newPage: ServiceSupport(),
          closedBuilder: (_, function) {
            return HomeListViewItem(
              imageUrl: "assets/svg/dv_ho_tro.svg",
              description: "Dịch vụ hỗ trợ",
              width: 56,
              imageSize: 56,
              onTab: function,
            );
          },
        ),
        // HomeListViewItem(
        //   imageUrl: "assets/svg/dv_ho_tro.svg",
        //   description: "Dịch vụ hỗ trợ",
        //   width: 56,
        //   imageSize: 56,
        //   onTab: () {
        //     Routes.sailor.navigate(Routes.serviceSupport);
        //   },
        // ),
        OpenContainerWrapper(
          // newPage: Utilities(),
          closedBuilder: (_, function) {
            return HomeListViewItem(
              imageUrl: "assets/svg/tien_ich_khac.svg",
              description: "Các tiện ích khác",
              width: 56,
              imageSize: 56,
              onTab: function,
            );
          },
        ),
        // HomeListViewItem(
        //   imageUrl: "assets/svg/tien_ich_khac.svg",
        //   description: "Các tiện ích khác",
        //   width: 56,
        //   imageSize: 56,
        //   onTab: () {
        //     Routes.sailor.navigate(Routes.utilitiesPage);
        //   },
        // ),
        //  TravelItem()
      ],
    );
  }
}
