import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class HomeListViewItem extends StatelessWidget {
  final String imageUrl;
  final String title;
  final String description;
  final double width;
  final double imageSize;
  final VoidCallback onTab;

  const HomeListViewItem({
    Key key,
    this.imageUrl,
    this.title,
    this.description,
    this.width,
    this.imageSize,
    this.onTab,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var gestureDetector = GestureDetector(
      onTap: onTab,
      child: Container(
        width: width,
        child: Column(
          children: <Widget>[
            Container(
              height: imageSize,
              width: imageSize,
              child: SvgPicture.asset(
                imageUrl,
                fit: BoxFit.cover,
              ),
            ),
            const SizedBox(height: 8),
            title == null
                ? SizedBox()
                : Padding(
                    padding: const EdgeInsets.only(bottom: 8),
                    child: Center(
                      child: Text(
                        title,
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 16),
                      ),
                    ),
                  ),
            Text(
              description,
              style: TextStyle(
                fontSize: 15,
              ),
              textAlign: TextAlign.center,
            ),
          ],
        ),
      ),
    );
    return gestureDetector;
  }
}
