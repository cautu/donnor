import 'package:animations/animations.dart';
import 'package:flutter/cupertino.dart';

class OpenContainerWrapper extends StatelessWidget {
  const OpenContainerWrapper({
    this.closedBuilder,
    this.transitionType = ContainerTransitionType.fade,
    this.newPage,
  });

  final OpenContainerBuilder closedBuilder;
  final ContainerTransitionType transitionType;
  final Widget newPage;

  @override
  Widget build(BuildContext context) {
    return OpenContainer(
      closedColor: Color(0xFFFFFFFF),
      closedElevation: 0,
      openElevation: 40,
      transitionType: transitionType,
      openBuilder: (BuildContext context, VoidCallback _) {
        return newPage;
      },
      tappable: false,
      closedBuilder: closedBuilder,
    );
  }
}
