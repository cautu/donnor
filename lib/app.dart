import 'package:flutter/material.dart';
import 'package:bright_sparqe/router/routes.dart';
import 'package:bright_sparqe/blocs/blocs.dart';
import 'package:bright_sparqe/pages/home.dart';

import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter/cupertino.dart';

class MainApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      blocs: [
        Bloc((i) => UserBloc()),
      ],
      child: MaterialApp(
        localizationsDelegates: [
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
          DefaultCupertinoLocalizations.delegate,
          GlobalCupertinoLocalizations.delegate, //
        ],
        // locale: Locale('vi', 'VN'),
        // supportedLocales: [const Locale('vi', "VN"), const Locale('en', 'US')],
        title: 'AIC KCB',
        theme: ThemeData(
          primarySwatch: Colors.blue,
          primaryColor: Colors.white,
          fontFamily: 'Lato',
        ),
        debugShowCheckedModeBanner: false,
        home: Home(),
        navigatorKey: Routes.sailor.navigatorKey,
        onGenerateRoute: Routes.sailor.generator(),
      ),
    );
  }
}
