import 'dart:async';
import 'package:flutter/material.dart';
import 'package:sailor/sailor.dart';

import 'package:webview_flutter/webview_flutter.dart';

class WebViewWidgetArgs extends BaseArguments {
  final String title;
  final String url;
  WebViewWidgetArgs({this.title, this.url});
}

class WebViewWidget extends StatelessWidget {
  final WebViewWidgetArgs args;

  final Completer<WebViewController> _controller =
      Completer<WebViewController>();

  WebViewWidget({
    @required this.args,
  });

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(args.title),
        ),
        body: WebView(
          initialUrl: args.url,
          javascriptMode: JavascriptMode.unrestricted,
          onWebViewCreated: (WebViewController webViewController) {
            _controller.complete(webViewController);
          },
        ));
  }
}
