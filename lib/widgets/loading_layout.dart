import 'package:flutter/material.dart';
import 'package:bright_sparqe/widgets/loading.dart';

class LoadingLayout extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: double.infinity,
      child: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Loading(),
            const SizedBox(
              height: 10.0,
            ),
            Text(
              'Đang tải dữ liệu',
              style: TextStyle(
                  color: Color(0xFF3A454D), fontSize: 15.0, letterSpacing: 0.6),
            ),
          ],
        ),
      ),
    );
  }
}
