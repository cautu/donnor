import 'package:flutter/material.dart';

const _EXPANDED_HEIGHT = 252.0;

class SliverHeader extends StatefulWidget {
  const SliverHeader({
    Key key,
    this.title,
    this.coverWidget,
    this.child,
    this.coverHeight: _EXPANDED_HEIGHT,
    this.controller,
  }) : super(key: key);

  final String title;
  final Widget coverWidget;
  final Widget child;
  final double coverHeight;
  final ScrollController controller;

  @override
  _SliverHeaderState createState() => _SliverHeaderState();
}

class _SliverHeaderState extends State<SliverHeader>
    with SingleTickerProviderStateMixin {
  ScrollController _controller;
  AnimationController _titleAnimController;
  Animation<Color> _titleAnim;
  double _minOffset;
  double _maxOffset;

  @override
  void initState() {
    _controller = widget.controller ?? ScrollController();
    _controller.addListener(_listener);

    _titleAnimController = AnimationController(vsync: this);
    _titleAnim = ColorTween(begin: Colors.white, end: Colors.black)
        .animate(_titleAnimController);

    _minOffset = widget.coverHeight - 2 * kToolbarHeight;
    _maxOffset = widget.coverHeight - kToolbarHeight;
    super.initState();
  }

  _listener() {
    final offset =
        _controller.offset.clamp(_minOffset, _maxOffset) - _minOffset;
    _titleAnimController.value = offset / kToolbarHeight;
  }

  @override
  Widget build(BuildContext context) {
    return CustomScrollView(
      controller: _controller,
      physics: BouncingScrollPhysics(),
      slivers: <Widget>[
        SliverAppBar(
          leading: AnimatedBuilder(
            animation: _titleAnim,
            builder: (context, child) => BackButton(color: _titleAnim.value),
          ),
          stretch: true,
          expandedHeight: widget.coverHeight,
          floating: false,
          pinned: true,
          flexibleSpace: FlexibleSpaceBar(
            centerTitle: true,
            title: AnimatedBuilder(
              animation: _titleAnim,
              builder: (context, child) {
                return Text(
                  widget.title ?? '',
                  style: TextStyle(
                    color: _titleAnim.value,
                    fontSize: 16.0,
                  ),
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                );
              },
            ),
            background: widget.coverWidget,
          ),
        ),
        SliverToBoxAdapter(
          child: widget.child,
        ),
      ],
    );
  }
}
