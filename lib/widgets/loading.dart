import 'package:flutter/material.dart';

class Loading extends StatelessWidget {
  final double width;
  final double height;
  Loading({this.width = 40.0, this.height = 40.0});
  @override
  Widget build(BuildContext context) {
    return Image.asset(
      'assets/images/loading_nice.gif',
      width: width,
      height: height,
    );
  }
}
