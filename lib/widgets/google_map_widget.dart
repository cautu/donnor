import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'dart:async';

class GoogleMapWidget extends StatefulWidget {
  final double latitude;
  final double longtitude;
  final String titleMarker;
  final String descriptionMarker;

  GoogleMapWidget(
      {this.latitude,
      this.longtitude,
      this.titleMarker,
      this.descriptionMarker});

  @override
  State<StatefulWidget> createState() => _GoogleMapWidgetState();
}

class _GoogleMapWidgetState extends State<GoogleMapWidget> {
  Map<MarkerId, Marker> markers = <MarkerId, Marker>{};
  // CameraPosition _position = CameraPosition(
  //   target: LatLng(37.42796133580664, -122.085749655962), // position of googleplex
  //   zoom: 14.4746,
  // );
  Completer<GoogleMapController> _controller = Completer();
  @override
  void initState() {
    super.initState();
    _setMarker();
  }

  void _setMarker() {
    // final GoogleMapController controller = await _controller.future;
    final markerIdVal = widget.titleMarker ?? "";
    final MarkerId markerId = MarkerId(markerIdVal);
    final Marker marker = Marker(
        markerId: markerId,
        position: LatLng(widget.latitude, widget.longtitude),
        infoWindow: InfoWindow(
            title: markerIdVal, snippet: widget.descriptionMarker ?? ""),
        onTap: () {
          print('marker');
        });

    setState(() {
      markers[markerId] = marker;
    });

    // controller.animateCamera(CameraUpdate.newCameraPosition(_position));
  }

  @override
  Widget build(BuildContext context) {
    CameraPosition _position = CameraPosition(
      bearing: 192.8334901395799,
      target: LatLng(widget.latitude, widget.longtitude),
      tilt: 59.440717697143555,
      zoom: 15,
    );

    return GoogleMap(
      markers: Set<Marker>.of(markers.values),
      mapType: MapType.normal,
      initialCameraPosition: _position,
      onMapCreated: (GoogleMapController controller) {
        _controller.complete(controller);
      },
    );
  }
}
