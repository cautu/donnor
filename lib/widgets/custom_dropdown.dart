import 'package:flutter/material.dart';

class CustomDrodown extends StatelessWidget {
  final Function onTap;
  final String value;
  final String placeholder;
  final String label;
  final IconData iconData;
  CustomDrodown(
      {this.onTap,
      this.value = '',
      this.placeholder = '',
      this.label = '',
      this.iconData = Icons.arrow_drop_down});
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Align(
          alignment: Alignment.centerLeft,
          child: Text(
            label,
            style: TextStyle(
                color: Color(0xFF3A454D),
                fontWeight: FontWeight.w600,
                fontSize: 14.0),
          ),
        ),
        const SizedBox(
          height: 10,
        ),
        GestureDetector(
          onTap: onTap,
          child: Container(
            width: MediaQuery.of(context).size.width,
            padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
            height: 48,
            decoration: BoxDecoration(
              color: Colors.white,
              border: Border.all(
                color: Color(0xFFDBDBDB),
              ),
            ),
            child: Row(
              children: <Widget>[
                Expanded(
                  child: placeholder.isNotEmpty && value.isEmpty
                      ? Text(placeholder,
                          style: TextStyle(
                              color: Colors.black38,
                              fontStyle: FontStyle.italic))
                      : Text(value, style: TextStyle(color: Color(0xFF3A454D))),
                ),
                Icon(iconData, color: Color(0xFF626874))
              ],
            ),
          ),
        )
      ],
    );
  }
}
