// import 'package:flutter/material.dart';
// import 'package:flutter/cupertino.dart';
// import 'package:aic_kcb/helpers/httpClient.dart';
// import 'package:aic_kcb/helpers/constants.dart';
// import 'package:aic_kcb/pages/hospital/filter/model.dart';

// class ServiceCity {
//   static Future<List<FilterModelComon>> fetch(
//       Map<String, String> params) async {
//     Uri uri =
//         Uri.https(Constants.DOMAIN, Constants.PATH_CITY_WITH_COUNTRY, params);
//     final result = await HttpClient.get(uri);
//     if (result != null) {
//       List<FilterModelComon> _listCity = List<FilterModelComon>();
//       result['data'].forEach((item) {
//         _listCity.add(FilterModelComon.fromJson(item));
//       });
//       return _listCity;
//     } else {
//       throw Exception('Failed to load data!');
//     }
//   }
// }

// class ListCity extends StatefulWidget {
//   final FilterModelComon city;
//   final String countryCode;
//   ListCity({this.countryCode, this.city});
//   @override
//   _ListCityState createState() => _ListCityState();
// }

// class _ListCityState extends State<ListCity> {
//   List<FilterModelComon> _data = [];
//   int _selectedIndex;
//   @override
//   void initState() {
//     super.initState();
//     _handleRefreshData();
//   }

//   _handleRefreshData() async {
//     Map<String, String> params = {"countryCode": widget.countryCode};
//     List<FilterModelComon> result = await ServiceCity.fetch(params);
//     if (mounted)
//       setState(() {
//         _data = result;
//         _selectedIndex = _getIndexInitial();
//       });
//   }

//   _getIndexInitial() {
//     if (_data != null && _data.length > 0 && widget.city != null) {
//       for (int i = 0; i < _data.length; i++) {
//         if (widget.city.code == _data[i].code) {
//           return i;
//         }
//       }
//     }
//     return 0;
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Container(
//       height: MediaQuery.of(context).size.height / 3,
//       child: Column(
//         children: <Widget>[
//           Padding(
//             padding: EdgeInsets.symmetric(vertical: 16.0, horizontal: 12.0),
//             child: Row(
//               children: <Widget>[
//                 GestureDetector(
//                   onTap: () {
//                     Navigator.pop(context);
//                   },
//                   child: Text(
//                     "Huỷ",
//                     style: TextStyle(color: Colors.green, fontSize: 16.0),
//                   ),
//                 ),
//                 Expanded(
//                     child: Center(
//                   child: Text(
//                     "Chọn tỉnh/thành phố",
//                     style: TextStyle(
//                         color: Colors.black,
//                         fontSize: 16.0,
//                         fontWeight: FontWeight.w600),
//                   ),
//                 )),
//                 GestureDetector(
//                   onTap: () {
//                     final result = (_data != null && _data.length > 0)
//                         ? _data[_selectedIndex]
//                         : null;

//                     Navigator.pop(context, result);
//                   },
//                   child: Text(
//                     "Chọn",
//                     style: TextStyle(color: Colors.green, fontSize: 16.0),
//                   ),
//                 )
//               ],
//             ),
//           ),
//           Expanded(
//               child: _data != null && _data.length > 0
//                   ? CupertinoPicker(
//                       scrollController: FixedExtentScrollController(
//                           initialItem: _selectedIndex ?? 0),
//                       backgroundColor: Colors.white,
//                       children: _data
//                           .map(
//                             (item) => Text(item.name,
//                                 style: TextStyle(color: Colors.black)),
//                           )
//                           .toList(),
//                       itemExtent: 40, //height of each item
//                       onSelectedItemChanged: (int index) {
//                         _selectedIndex = index;
//                       },
//                     )
//                   : CupertinoActivityIndicator()),
//         ],
//       ),
//     );
//   }
// }
