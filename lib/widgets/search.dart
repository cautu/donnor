import 'package:flutter/material.dart';

class Search extends StatelessWidget {
  final circleRadius;
  final Function onChange;

  const Search({Key key, this.circleRadius = false, this.onChange})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Theme(
      child: TextFormField(
        onChanged: onChange,
        decoration: InputDecoration(
          filled: true,
          fillColor: Colors.white,
          hintText: "Tìm kiếm",
          prefixIcon: Icon(Icons.search),
          enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(color: Color(0xFFC8CACF), width: 2),
            borderRadius: BorderRadius.circular(circleRadius ? 24 : 8),
          ),
          contentPadding:
              const EdgeInsets.only(left: 14.0, bottom: 8.0, top: 8.0),
          focusedBorder: OutlineInputBorder(
            borderSide: const BorderSide(color: Color(0xFFC8CACF), width: 1.5),
            borderRadius: BorderRadius.circular(circleRadius ? 24 : 8),
          ),
        ),
      ),
      data: Theme.of(context).copyWith(
        primaryColor: Colors.black,
      ),
    );
  }
}
