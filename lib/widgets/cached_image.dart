// import 'package:bright_sparqe/helpers/constants.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

class CachedImage extends StatelessWidget {
  const CachedImage({
    Key key,
    @required this.url,
    this.fit: BoxFit.cover,
    this.width,
    this.height,
  }) : super(key: key);

  final String url;
  final BoxFit fit;
  final double width;
  final double height;
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: width,
      height: height,
      child: CachedNetworkImage(
        imageUrl: url ?? '',
        // placeholder: (context, url) =>
        //     CircularProgressIndicator(strokeWidth: 3.0),
        placeholder: (_, url) => Image.asset("assets/images/loading4.gif"),
        // errorWidget: (_, __, ___) => Image.asset(Constants.IMG_NOT_FOUND),
        fit: BoxFit.cover,
      ),
    );
  }
}
