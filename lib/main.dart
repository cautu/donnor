import 'package:bright_sparqe/router/routes.dart';
import 'package:flutter/material.dart';
import 'app.dart';

void main() async {
  Routes.createRoutes();
  runApp(MainApp());
}
